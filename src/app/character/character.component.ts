import { Component, OnInit } from '@angular/core';
import { Character } from 'src/Models/character';
import { CharacterService } from '../character.service'

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

  character: Character;

  constructor(private characterService: CharacterService) { }

  ngOnInit(): void {
    this.characterService.generateCharacter()
      .subscribe(char => this.character = char);
  }

  newCharacter(): void {
    this.characterService.generateCharacter()
      .subscribe( char => this.character = char);
  }

  pickArticle(): String {
    return /[aeiouAEIOU]/.test(this.character.race.name[0]) ? "an" : "a";
  }

  abilityModifier(score: Number): Number {
    let modifier = 0
    switch(score) {
      case 8:
      case 9:
        modifier = -1; break;
      case 10:
      case 11:
        modifier = 0; break;
      case 12:
      case 13:
        modifier = 1; break;
      case 14:
      case 15:
        modifier = 2; break;
      default: modifier = 0; break;
    }
    return modifier
  }

}
