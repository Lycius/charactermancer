import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ABILITY_SCORES } from 'src/Data/ability-scores';
import { Race } from '../Models/race';
import { RACES } from 'src/Data/races';
import { CharAbility } from 'src/Models/char-ability';
import { Character } from 'src/Models/character';
import { ABILITIES } from '../Data/abilities';
import { CharClass } from 'src/Models/char-class';
import { CLASSES } from 'src/Data/char-classes';
import { Background } from '../Models/background';
import { BACKGROUNDS } from 'src/Data/backgrounds';
import { HairColor } from 'src/Models/hair-color';
import { HAIR_COLORS } from 'src/Data/hair-colors';
import { HairStyle } from 'src/Models/hair-style';
import { HAIR_STYLES } from 'src/Data/hair-styles';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor() { }

  generateCharacter(): Observable<Character> {
    let chartmp: Character = {
      abilities: [],
      race: null,
      charClass: null,
      background: null,
      hairColor: null,
      hairStyle: null,
    };

    chartmp.abilities = this.generateAbilities();
    chartmp.race = this.generateRace();
    chartmp.charClass = this.generateClass();
    chartmp.background = this.generateBackground();
    chartmp.hairColor = this.generateHairColor();
    chartmp.hairStyle = this.generateHairStyle();
    console.log(chartmp);
    return of(chartmp)
  }

  generateAbilities(): CharAbility[] {
    let charAbilities: CharAbility[] = []
    let abilityScores = ABILITY_SCORES;

    ABILITIES.forEach(e => {
      let abiltmp: CharAbility = { ability: { name: "" }, score: 0 };
      abiltmp.ability.name = e.name;
      abiltmp.score = abilityScores[Math.floor(Math.random() * abilityScores.length)]
      abilityScores = abilityScores.filter(x => x != abiltmp.score);
      charAbilities.push(abiltmp);
    });

    return charAbilities
  }

  generateRace(): Race {
    return RACES[Math.floor(Math.random() * RACES.length)];
  }

  generateClass(): CharClass {
    return CLASSES[Math.floor(Math.random() * CLASSES.length)];
  }

  generateBackground(): Background {
    return BACKGROUNDS[Math.floor(Math.random() * BACKGROUNDS.length)];
  }

  generateHairColor(): HairColor {
    return HAIR_COLORS[Math.floor(Math.random() * HAIR_COLORS.length)];
  }

  generateHairStyle(): HairStyle {
    return HAIR_STYLES[Math.floor(Math.random() * CLASSES.length)];
  }
}
