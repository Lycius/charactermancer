import { CharAbility } from './char-ability'
import { Background } from './background';
import { CharClass } from './char-class';
import { HairColor } from './hair-color';
import { HairStyle } from './hair-style';
import { Race } from './race';

export interface Character {
    abilities: CharAbility[],
    background: Background,
    charClass: CharClass,
    race: Race,
    hairStyle: HairStyle,
    hairColor: HairColor
}