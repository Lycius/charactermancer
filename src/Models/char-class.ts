export interface CharClass {
    name: String,
    description: String
}