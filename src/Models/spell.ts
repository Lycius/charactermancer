export interface Spell {
    level: Number,
    name: String,
    castingTime: String,
    duration: String,
    ranger: String,
    save: String,
    effect: String
}