export interface Background {
    name: String,
    description: String,
    feature: String,
    proficiencies: String[]
}