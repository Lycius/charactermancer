import { Ability } from "./ability";

export interface CharAbility {
    ability: Ability,
    score: Number
}