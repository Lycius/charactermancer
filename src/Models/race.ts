export interface Race {
    name: String,
    description: String
}