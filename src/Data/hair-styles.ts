import { HairStyle } from "src/Models/hair-style";

export const HAIR_STYLES: HairStyle[] = [
    { style: "Bob" },
    { style: "Pixie Cut" },
    { style: "Mohawk" },
    { style: "Fauxhawk" },
    { style: "Bun" },
    { style: "Top knot" },
    { style: "Buzz Cut" },
    { style: "Pigtails" },
    { style: "Ponytail" },
    { style: "Shoulder length Cut" },
    { style: "Back length Cut" },
    { style: "Short" }
]