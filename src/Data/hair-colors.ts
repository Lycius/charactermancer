import { HairColor } from "src/Models/hair-color";

export const HAIR_COLORS: HairColor[] = [
    { color: "Blonde" },
    { color: "Dirty Blonde" },
    { color: "Dark Brown" },
    { color: "Light Brown" },
    { color: "Red" },
    { color: "Orange" },
    { color: "Green" },
    { color: "Blue" },
    { color: "Violet" },
    { color: "Rainbow" }
]