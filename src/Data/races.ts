import { Race } from "../Models/race";

export const RACES: Race[] = [
    { name: "Dragonborn", description: "" },
    { name: "Dwarf", description: "" },
    { name: "Elf", description: "" },
    { name: "Gnome", description: "" },
    { name: "Half-Elf", description: "" },
    { name: "Halfling", description: "" },
    { name: "Half-Orc", description: "" },
    { name: "Human", description: "" },
    { name: "Tiefling", description: "" },
]