import { CharClass } from '../Models/char-class'

export const CLASSES: CharClass[] = [
    { name: "Barbarian", description: ""},
    { name: "Bard", description: ""},
    { name: "Cleric", description: "" },
    { name: "Druid", description: "" },
    { name: "Fighter", description: "" },
    { name: "Monk", description: "" },
    { name: "Paladin", description: "" },
    { name: "Ranger", description: "" },
    { name: "Rogue", description: "" },
    { name: "Sorcerer", description: "" },
    { name: "Warlock", description: "" },
    { name: "Wizard", description: "" }
]