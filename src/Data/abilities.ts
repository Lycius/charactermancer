import { Ability } from '../Models/ability'

export const ABILITIES: Ability[] = [
    { name: "Strength" },
    { name: "Dexterity" },
    { name: "Constitution" },
    { name: "Intelligence" },
    { name: "Wisdom" },
    { name: "Charisma" }
]