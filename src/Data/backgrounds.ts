import { Background } from '../Models/background'

export const BACKGROUNDS: Background[] = [
    { name: "Acolyte", description: "", feature: "Shelter of the Faithful", proficiencies: ["Insight", "Religion"] },
    { name: "Charlatan", description: "", feature: "False Identity", proficiencies: ["Deception", "Sleight of Hand"] },
    { name: "Criminal/Spy", description: "", feature: "Criminal Contact", proficiencies: ["Deception", "Stealth"] },
    { name: "Entertainer", description: "", feature: "By Popular Demand", proficiencies: ["Acrobatics", "Performance"] },
    { name: "Folk Hero", description: "", feature: "Rustic Hospitality", proficiencies: ["Animal Handling", "Survival"] },
    { name: "Gladiator", description: "", feature: "By Popular Demand", proficiencies: ["Acrobatics", "Performance"] },
    { name: "Guild Artisan", description: "", feature: "Guild Membership", proficiencies: ["Insight", "Persuasion"] },
    { name: "Hermit", description: "", feature: "Discovery", proficiencies: ["Medicine", "Religion"] },
    { name: "Knight", description: "", feature: "Retainers", proficiencies: ["History", "Persuasion"] },
    { name: "Noble", description: "", feature: "Position of Privilege", proficiencies: ["History", "Persuasion"] },
    { name: "Outlander", description: "", feature: "Wanderer", proficiencies: ["Athletics", "Survival"] },
    { name: "Pirate", description: "", feature: "Bad Reputation", proficiencies: ["Athletics", "Perception"] },
    { name: "Sage", description: "", feature: "Researcher", proficiencies: ["Arcana", "History"] },
    { name: "Sailor", description: "", feature: "Ship's Passage", proficiencies: ["Athletics", "Perception"] },
    { name: "Soldier", description: "", feature: "Military Rank", proficiencies: ["Athletics", "Intimidation"] },
    { name: "Urchin", description: "", feature: "City Secrets", proficiencies: ["Sleight of Hand", "Stealth"] }

]